#!/bin/bash

SIMGRID_PATH="${SIMGRID_PATH:=/opt/simgrid}"
WORKSPACE="${WORKSPACE:=$PWD}"
CORES=$(grep ^cpu\\scores /proc/cpuinfo | uniq |  awk '{print $4}')

cd "$WORKSPACE/src" || exit 1

make clean
make -j $CORES || exit 2

smpirun -hostfile ../cluster_hostfile.txt -platform ../cluster_fattree.xml ./qs
